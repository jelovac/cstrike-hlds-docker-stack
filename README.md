# Counter Strike 1.6 HLDS Docker Stack

## Setup

- Clone the repository
- Copy .env.example to .env
- Configure the .env
- Start the stack: `docker-compose up -d`
